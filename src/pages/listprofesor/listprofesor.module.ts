import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListprofesorPage } from './listprofesor';

@NgModule({
  declarations: [
    ListprofesorPage,
  ],
  imports: [
    IonicPageModule.forChild(ListprofesorPage),
  ],
})
export class ListprofesorPageModule {}
