import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/*
 * Generated class for the AlumnoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-alumno',
  templateUrl: 'alumno.html',
})
export class AlumnoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  // pie

  num1 = 0.6;
  public pieChartLabels:string[] = ['Aprobados', 'Reprobados'];
  public pieChartData:Number[] = [60,40];
  public pieChartType:string = 'pie';

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AlumnoPage');
  }

}
