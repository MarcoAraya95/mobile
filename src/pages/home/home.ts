import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlumnoPage } from '../alumno/alumno';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('rut') rut;
  @ViewChild('password') password;

  constructor(public navCtrl: NavController) {

    
  }

  login() {
    this.navCtrl.push(AlumnoPage);
  }

}