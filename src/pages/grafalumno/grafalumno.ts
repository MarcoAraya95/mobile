import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GrafalumnoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-grafalumno',
  templateUrl: 'grafalumno.html',
})
export class GrafalumnoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  num1 = 0.6;
  public lineChartLabels:string[] = ['2013', '2014','2015','2016','2017','2018'];
  public lineChartData:any[] = [
    {data: [45,40,42,47,52,50], label: 'Yo'},
    {data: [50,45,47,45,47,48], label: 'Resto'}
  ];
  public lineChartType:string = 'line';


  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad GrafalumnoPage');
  }

}
